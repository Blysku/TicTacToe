public class MainTicTacToe {
    public static void main(String[] args) {
        TicTacToe game = new TicTacToe();
        try {
            game.run(3,3, new KeyboardReader(), System.out);
        } catch (MatrixNotSquareException e) {
            e.printStackTrace();
        } catch (NotAPlayerException e) {
            e.printStackTrace();
        }
    }
}
