import java.io.PrintStream;
import java.util.Scanner;

public class TicTacToe {

    public int run(int neededToWin, int boardStartingSize, KeyboardReader keyboardReader, PrintStream out) throws MatrixNotSquareException, NotAPlayerException {
        String player = "X";
        boolean turn = true;
        int currentBoardSize = boardStartingSize;
        String[][] fields = initFields(currentBoardSize);
        String board;
        while (!checkWinConditions(fields, neededToWin, player)) {
            if (turn) {
                player = "X";
            } else {
                player = "O";
            }
            board = updateBoard(fields, currentBoardSize);
            System.out.println(board);

            int rowNumber = -1;
            int colNumber = -1;

            do {
                System.out.println("Ruch " + player + ".");

                System.out.print("Podaj numer kolumny.\n> ");

                rowNumber = keyboardReader.nextInt();
                if (isBoardTooSmall(fields, rowNumber)) {
                    fields = extendFields(fields, rowNumber);
                }

                System.out.print("\nPodaj numer wiersza.\n> ");

                colNumber = keyboardReader.nextInt();
                if (isBoardTooSmall(fields, colNumber)) {
                    fields = extendFields(fields, colNumber);
                }
                if (isFieldTaken(fields, rowNumber, colNumber)) {
                    System.out.println("Pole zajete, sprobuj jeszcze raz.");
                }
            } while (isFieldTaken(fields, rowNumber, colNumber));

            fields = fillInField(fields, rowNumber, colNumber, player);
            if (checkWinConditions(fields, neededToWin, player) || isDraw(fields)) {
                break;
            } else {
                turn = !turn;
            }
        }
        if (checkWinConditions(fields, neededToWin, player)) {
            board = updateBoard(fields, currentBoardSize);
            System.out.println(board);
            out.println("Gracz " + player + " zwyciezyl. Gratulacje!");
            return 1;
        }
        if (isDraw(fields)) {
            board = updateBoard(fields, currentBoardSize);
            System.out.println(board);
            out.println("Remis!");
            return 0;
        }

        return -1;
    }

    public boolean isBoardTooSmall(String[][] fields, int newSize) {
        if (fields == null) {
            return true;
        }
        if (fields.length == 0) {
            return true;
        }
        return fields.length <= newSize;
    }

    public String[][] extendFields(String[][] fields, int increaseBy) throws NullPointerException, IllegalArgumentException, MatrixNotSquareException {
        if (fields == null) {
            throw new NullPointerException();
        }
        if (increaseBy < 0) {
            throw new IllegalArgumentException();
        }
        if (increaseBy == 0) {
            return fields;
        }
        for (String[] sArr : fields) {
            if (sArr.length != fields.length){
                throw new MatrixNotSquareException();
            }
        }
        int currentBoardSize = fields.length + increaseBy;
        String[][] newBoard = initFields(currentBoardSize);
        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < fields[i].length; j++) {
                newBoard[i][j] = fields[i][j];
            }
        }
        return newBoard;
    }

    public boolean isFieldTaken(String[][] fields, int row, int col) throws ArrayIndexOutOfBoundsException, NullPointerException {
        if (row < 0 || col < 0){
            throw new ArrayIndexOutOfBoundsException();
        }
        if (row > fields.length || col > fields[0].length){
            throw new ArrayIndexOutOfBoundsException();
        }
        if (fields[row][col] == null){
            throw new NullPointerException();
        }
        return !fields[row][col].equals(" ");
    }

    public String[][] fillInField(String[][] fields, int row, int col, String player) {
        String[][] newBoard = fields.clone();
        newBoard[row][col] = player;
        return newBoard;
    }

    public boolean checkWinConditions(String[][] fields, int neededToWin, String player) throws NullPointerException, NotAPlayerException {
        if (player == null || fields == null){
            throw new NullPointerException();
        }
        if (!player.equals("X") && !player.equals("O")){
            throw new NotAPlayerException();
        }
        if (neededToWin == 0){
            return true;
        }
        int rowWin;
        int colWin;
        int lDiagonalWin;
        int rDiagonalWin;

        for (int i = 0; i < fields.length; i++) {
            rowWin = 1;
            colWin = 1;
            lDiagonalWin = 1;
            rDiagonalWin = 1;

            for (int j = 0; j < fields[i].length - 1; j++) {
                int lastIndex = fields.length - 1;
                if (fields[i][j].equals(player) && fields[i][j].equals(fields[i][j + 1])) {
                    rowWin++;
                } else {
                    rowWin = 1;
                }
                if (fields[j][i].equals(player) && fields[j][i].equals(fields[j + 1][i])) {
                    colWin++;
                } else {
                    colWin = 1;
                }
                if (fields[j][j].equals(player) && fields[j][j].equals(fields[j + 1][j + 1])) {
                    lDiagonalWin++;
                } else {
                    lDiagonalWin = 1;
                }
                if (fields[j][lastIndex - j].equals(player) && fields[j][lastIndex - j].equals(fields[j + 1][lastIndex - j - 1])) {
                    rDiagonalWin++;
                } else {
                    rDiagonalWin = 1;
                }
                if (rowWin == neededToWin || colWin == neededToWin || lDiagonalWin == neededToWin || rDiagonalWin == neededToWin) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isDraw(String[][] fields) {
        boolean isDraw = true;
        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < fields[i].length; j++) {
                if (fields[i][j].equals(" ")) {
                    isDraw = false;
                }
            }
        }
        return isDraw;
    }

    public String[][] initFields(int boardSize) {
        String[][] board = new String[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                board[i][j] = " ";
            }
        }
        return board;
    }

    public String updateBoard(String[][] board, int currentBoardSize) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < currentBoardSize; i++) {
            for (int j = 0; j < currentBoardSize; j++) {
                if (j == 0) {
                    sb.append(" ");
                }
                sb.append(board[i][j]);
                if (j != currentBoardSize - 1) {
                    sb.append(" | ");
                }
            }
            sb.append("\n");
            if (i != currentBoardSize - 1) {
                for (int j = 0; j < currentBoardSize - 1; j++) {
                    sb.append("---+");
                    if (j == currentBoardSize - 2) {
                        sb.append("---");
                    }
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
