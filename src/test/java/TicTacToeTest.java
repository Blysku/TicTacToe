import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class TicTacToeTest {
    private TicTacToe game;
    KeyboardReader keyboardReader;
    PrintStream out;
    private boolean mockInitialized = false;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        if (!mockInitialized) {
            MockitoAnnotations.initMocks(this);
            mockInitialized = true;
        }
        game = new TicTacToe();
        keyboardReader = mock(KeyboardReader.class);
        out = mock(PrintStream.class);
    }

    @Test
    @Parameters
    public void testPlayXWin(int neededToWin, int boardStartingSize) throws NotAPlayerException, MatrixNotSquareException {
        when(this.keyboardReader.nextInt()).thenReturn(0, 0, 1, 1, 0, 1, 2, 2, 0, 2, 5, 5, 0, 3, 4, 4, 0, 4);
        game.run(neededToWin, boardStartingSize, keyboardReader, out);
        verify(out).println("Gracz X zwyciezyl. Gratulacje!");
    }

    private Object[] parametersForTestPlayXWin() {
        return new Object[]{
                new Object[]{3, 3},
                new Object[]{5, 10}
        };
    }

    @Test
    @Parameters
    public void testPlayOWin(int neededToWin, int boardStartingSize) throws NotAPlayerException, MatrixNotSquareException {
        when(this.keyboardReader.nextInt()).thenReturn(5, 1, 0, 0, 1, 1, 0, 1, 2, 2, 0, 2, 5, 5, 0, 3, 4, 4, 0, 4);
        game.run(neededToWin, boardStartingSize, keyboardReader, out);
        verify(out).println("Gracz O zwyciezyl. Gratulacje!");
    }

    private Object[] parametersForTestPlayOWin() {
        return new Object[]{
                new Object[]{3, 3},
                new Object[]{5, 10}
        };
    }

    @Test
    @Parameters
    public void testPlayDraw(int neededToWin, int boardStartingSize) throws NotAPlayerException, MatrixNotSquareException {
        when(this.keyboardReader.nextInt()).thenReturn(1, 1, 0, 0, 2, 0, 0, 2, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0);
        game.run(neededToWin, boardStartingSize, keyboardReader, out);
        verify(out).println("Remis!");
    }

    private Object[] parametersForTestPlayDraw() {
        return new Object[]{
                new Object[]{3, 3},
        };
    }

    @Test
    public void testIsBoardTooSmallForNull() {
        String[][] board = null;
        assertTrue(game.isBoardTooSmall(board, 0));
    }

    @Test
    @Parameters
    public void testIsBoardTooSmall(String[][] board, int newSize, boolean testResult) {
        assertEquals(testResult, game.isBoardTooSmall(board, newSize));
    }

    private Object[] parametersForTestIsBoardTooSmall() {
        return new Object[]{
                new Object[]{new String[5][5], 6, true},
                new Object[]{new String[5][5], 5, true},
                new Object[]{new String[0][0], 0, true},
                new Object[]{new String[5][5], 4, false},
                new Object[]{new String[5][5], 0, false},

        };
    }

    @Test(expected = NullPointerException.class)
    public void testExtendFieldsForNull() throws MatrixNotSquareException {
        String[][] board = null;
        String[][] boardToTest = game.extendFields(board, 5);

        assertNull(boardToTest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExtendFieldsForNegativeValue() throws MatrixNotSquareException {
        String[][] board = new String[5][5];
        String[][] boardToTest = game.extendFields(board, -1);

        assertNull(boardToTest);
    }

    @Test(expected = MatrixNotSquareException.class)
    public void testExtendFieldsForBoardNotSquare() throws MatrixNotSquareException {
        String[][] board = new String[5][8];

        String[][] boardToTest = game.extendFields(board, 1);

        assertNull(boardToTest);
    }

    @Test
    @Parameters
    public void testExtendFieldsCheckSize(String[][] board, int addSize, int testResult) throws MatrixNotSquareException {
        String[][] boardToTest = game.extendFields(board, addSize);

        assertEquals(testResult, boardToTest.length);
    }

    private Object[] parametersForTestExtendFieldsCheckSize() {
        return new Object[]{
                new Object[]{new String[5][5], 2, 7},
                new Object[]{new String[0][0], 5, 5},
                new Object[]{new String[5][5], 0, 5},
                new Object[]{new String[3][3], 11, 14},
                new Object[]{new String[0][0], 0, 0},
        };
    }

    @Test
    public void testExtendFieldsCheckIfOriginalArrayIsNotChanged() throws MatrixNotSquareException {
        String[][] originalBoard = {{"raz", "dwa", "trzy"},
                {"cztery", "piec", "szesc"},
                {"siedem", "osiem", "dziewiec"}};
        String[][] board = originalBoard.clone();
        game.extendFields(board, 0);

        for (int i = 0; i < originalBoard.length; i++) {
            assertEquals(originalBoard[i], board[i]);
        }
    }

    @Test
    @Parameters
    public void testExtendFieldsCheckCopyingValues(String[][] board) throws MatrixNotSquareException {
        String[][] boardToTest = game.extendFields(board, 0);

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                assertEquals(board[i][j], boardToTest[i][j]);
            }
        }
    }

    private Object[] parametersForTestExtendFieldsCheckCopyingValues() {
        return new Object[]{
                new Object[]{new String[][]{{" ", " ", " "},
                        {" ", " ", " "},
                        {" ", " ", " "}}},
                new Object[]{new String[][]{{"raz", "dwa", "trzy"},
                        {"cztery", "piec", "szesc"},
                        {"siedem", "osiem", "dziewiec"}}},
                new Object[]{new String[][]{{"X", " ", "O"},
                        {"O", "O", "X"},
                        {" ", " ", "X"}}},
        };
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    @Parameters
    public void testIsFieldTakenArrayIndexOutOfBoundsException(String[][] board, int row, int col) {
        Boolean result = game.isFieldTaken(board, row, col);

        assertNull(result);
    }

    private Object[] parametersForTestIsFieldTakenArrayIndexOutOfBoundsException() {
        return new Object[]{
                new Object[]{new String[][]{{"raz", "dwa", "trzy"},
                        {"cztery", "piec", "szesc"},
                        {"siedem", "osiem", "dziewiec"}},
                        -5, -5},
                new Object[]{new String[][]{{"raz", "dwa", "trzy"},
                        {"cztery", "piec", "szesc"},
                        {"siedem", "osiem", "dziewiec"}},
                        5, 5},
        };
    }

    @Test
    public void testIsFieldTakenNullException() {
        thrown.expect(NullPointerException.class);
        String[][] board = null;
        Boolean result = game.isFieldTaken(board, 1, 1);

        assertNull(result);
    }

    @Test
    @Parameters
    public void testIsFieldTaken(String[][] board, int row, int col, boolean testResult) {
        assertEquals(testResult, game.isFieldTaken(board, row, col));
    }

    private Object[] parametersForTestIsFieldTaken() {
        return new Object[]{
                new Object[]{new String[][]{{"X", " ", "O"},
                        {"O", "O", "X"},
                        {" ", " ", "X"}},
                        0, 0, true},
                new Object[]{new String[][]{{"X", " ", "O"},
                        {"O", "O", "X"},
                        {" ", " ", "X"}},
                        2, 0, false},
                new Object[]{new String[][]{{"X", " ", "O"},
                        {"O", "O", "X"},
                        {" ", " ", "X"}},
                        0, 2, true},
                new Object[]{new String[][]{{"X", " ", "O"},
                        {"O", "O", "X"},
                        {" ", " ", "X"}},
                        0, 1, false},
        };
    }

    @Test
    public void testCheckWinConditionsNullPointerExceptions() throws NotAPlayerException {
        thrown.expect(NullPointerException.class);
        Boolean result = game.checkWinConditions(null, 0, "X");

        assertNull(result);
    }

    @Test
    public void testCheckWinConditionsForNotAPlayerException() throws NotAPlayerException {
        thrown.expect(NotAPlayerException.class);
        Boolean result = game.checkWinConditions(new String[][]{{"X", " ", "O"},
                        {"O", "O", "X"},
                        {" ", " ", "X"}},
                3, " ");

        assertNull(result);
    }

    @Test
    @Parameters
    public void testCheckWinConditions(String[][] board, int neededToWin, String player, boolean expectedResult) throws NotAPlayerException {
        assertEquals(expectedResult, game.checkWinConditions(board, neededToWin, player));
    }

    private Object[] parametersForTestCheckWinConditions() {
        return new Object[]{
                new Object[]{new String[][]{
                        {"X", " ", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", " ", "O", " ", " ", " "},
                        {" ", " ", "O", " ", "X", " "},
                        {" ", " ", " ", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        3, "X", false},
                new Object[]{new String[][]{
                        {"X", " ", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", " ", "X", " ", " ", " "},
                        {" ", " ", "X", " ", "X", " "},
                        {" ", " ", " ", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        3, "O", false},
                new Object[]{new String[][]{
                        {"X", " ", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", " ", "X", " ", " ", " "},
                        {" ", " ", "X", " ", "X", " "},
                        {" ", " ", " ", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        3, "X", true},
                new Object[]{new String[][]{
                        {"X", " ", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", " ", "X", " ", " ", " "},
                        {" ", " ", "X", " ", "X", " "},
                        {" ", " ", "X", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        4, "X", true},
                new Object[]{new String[][]{
                        {"X", " ", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", " ", " ", " ", " ", " "},
                        {" ", "X", "X", "X", "X", " "},
                        {" ", " ", " ", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        4, "X", true},
                new Object[]{new String[][]{
                        {"X", " ", "X", "X", " ", " "},
                        {" ", "X", "X", " ", "X", " "},
                        {"X", " ", " ", " ", " ", " "},
                        {" ", "X", "X", " ", "X", " "},
                        {" ", "X", " ", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        4, "X", true},
                new Object[]{new String[][]{
                        {"X", "X", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", "X", " ", " ", " ", " "},
                        {" ", "X", "X", " ", "X", " "},
                        {" ", " ", " ", "X", " ", "X"},
                        {"O", "O", "X", "X", " ", " "},
                },
                        3, "X", true},
                new Object[]{new String[][]{
                        {"X", " ", " ", "X", " ", " "},
                        {" ", "O", "X", " ", " ", " "},
                        {" ", " ", "X", " ", " ", " "},
                        {" ", " ", "X", " ", "X", " "},
                        {" ", " ", "X", " ", " ", " "},
                        {"O", "O", "X", "X", "O", " "},
                },
                        5, "X", true},
        };
    }
}

