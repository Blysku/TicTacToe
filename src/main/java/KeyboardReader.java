import java.util.Scanner;

public class KeyboardReader {
    private Scanner scanner;

    public KeyboardReader() {
        this.scanner = new Scanner(System.in);
    }

    public int nextInt() {
        return this.scanner.nextInt();
    }
}